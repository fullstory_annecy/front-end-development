document.addEventListener("DOMContentLoaded", function() {
    // BURGER
    document.querySelector('nav button').addEventListener('click',(e) => {
        document.body.classList.toggle('open');
        if(document.body.classList.contains('open')) {
            document.querySelector('nav button').innerHTML = 'close';
        }else {
            document.querySelector('nav button').innerHTML = 'menu';
        }
    });
    // FORM
    let form = document.getElementById('form');
    form.noValidate = true;

    // set handler to validate the form
    // onsubmit used for easier cross-browser compatibility
    form.onsubmit = validateForm;
});