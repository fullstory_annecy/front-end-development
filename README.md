# Front End development
### 1. Git
```
git --version
```
result :
```
git version 2.17.1 (Apple Git-112)
```
if you don't see a version number use this link to download the latest version
https://git-scm.com/download/mac

### 2. Node & npm
```
node -v
```
result :
```
v10.5.0
```
if you don't see a version number use this link to download the latest version
https://nodejs.org/en/

## Installation
```
npm install
```

## Launch local serveur
```
npm run serve
```

